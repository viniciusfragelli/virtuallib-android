package br.com.virtualtechnology.virtuallib;

import android.content.Context;
import android.graphics.Point;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;

import br.com.virtualtechnology.virtuallib.bases.BaseActivity;

/**
 * Created by vinic on 03/11/2017.
 */

public class VirtualApplication {

    private static VirtualApplication virtualAplication = null;

    public static VirtualApplication getInstance(){
        if(virtualAplication == null)virtualAplication = new VirtualApplication();
        return virtualAplication;
    }

    public BaseActivity activity = null;
    public Fragment fragment = null;

    public boolean isBackNative = false;

    public boolean isClosedAfterEndBack = false;

    public Context context = null;

    private Integer deviceWidch = null;
    private Integer deviceHeight = null;

    public int getDeviceWidch(){
        if(deviceWidch == null){
            getPixels();
            if(deviceWidch == null)return 0;
            else return deviceWidch;
        }else{
            return deviceWidch;
        }
    }

    public int getDeviceHeight(){
        if(deviceHeight == null){
            getPixels();
            if(deviceHeight == null)return 0;
            else return deviceHeight;
        }else{
            return deviceHeight;
        }
    }

    private void getPixels(){
        if(activity != null) {
            Display display = VirtualApplication.getInstance().activity.getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            deviceWidch = size.x;
            deviceHeight = size.y;
        }
    }
}
