package br.com.virtualtechnology.virtuallib.animete;

import android.os.Build;
import android.transition.TransitionManager;
import android.view.ViewGroup;

/**
 * Created by vinic on 09/10/2017.
 */

public abstract class Animate {
    public static void animeteView(ViewGroup view){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.beginDelayedTransition(view);
        }
    }
}
