package br.com.virtualtechnology.virtuallib.bases;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import br.com.virtualtechnology.virtuallib.VirtualApplication;
import br.com.virtualtechnology.virtuallib.runs.runProcessAsyncOnline;
import br.com.virtualtechnology.virtuallib.runs.utils.Task;

/**
 * Created by vinic on 03/11/2017.
 */

public abstract class BaseActivity extends AppCompatActivity{

    protected void startTaskAsync(final Task task){
        new runProcessAsyncOnline(task,this).start();
    }

    protected void showAlert(final String titulo, final String text){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(getApplicationContext()).setTitle(titulo).setMessage(text).setPositiveButton("OK", null).setCancelable(false).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        onBackPressed(VirtualApplication.getInstance().isBackNative);
    }

    public void onBackPressed(boolean isNative) {
        if(!isNative) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                getSupportFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        }else {
            super.onBackPressed();
        }
//        super.onBackPressed();
//        Log.e("Teste", "Size lista frag: "+getSupportFragmentManager().getFragments().size());
//        Log.e("Teste", "Size entry frag: "+getSupportFragmentManager().getBackStackEntryCount());
//        if(getSupportFragmentManager().getBackStackEntryCount() > 1) {
//            //final FragmentManager fragmentManager = getSupportFragmentManager();
//            //fragmentManager.beginTransaction().remove(fragmentManager.getFragments().get(fragmentManager.getFragments().size()-1)).commit();
//            getSupportFragmentManager().popBackStack();
//            android.app.Fragment myFragment = getFragmentManager().findFragmentByTag("Carregando");
//            if (myFragment != null && myFragment.isVisible()) {
//                getSupportFragmentManager().popBackStack();
//            }
//            //super.onBackPressed();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        VirtualApplication.getInstance().activity = this;
        VirtualApplication.getInstance().context = getApplicationContext();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        VirtualApplication.getInstance().activity = this;
        VirtualApplication.getInstance().context = getApplicationContext();
    }

    @Override
    protected void onResume() {
        super.onResume();
        VirtualApplication.getInstance().activity = this;
        VirtualApplication.getInstance().context = getApplicationContext();
    }

    protected void setupToolBar(int id, String title, boolean navgationBack){
        Toolbar toolbar = (Toolbar) findViewById(id);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        ActionBar ac = getSupportActionBar();
        ac.setDisplayHomeAsUpEnabled(navgationBack);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onActionBarHomePressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onActionBarHomePressed(){
        VirtualApplication.getInstance().activity.onBackPressed();
    }
}