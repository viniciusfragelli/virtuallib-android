package br.com.virtualtechnology.virtuallib.bases;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import br.com.virtualtechnology.virtuallib.VirtualApplication;
import br.com.virtualtechnology.virtuallib.runs.runProcessAsyncOnline;
import br.com.virtualtechnology.virtuallib.runs.utils.Task;

/**
 * Created by vinic on 09/10/2017.
 */

public abstract class BaseFragments extends Fragment{

    protected Bundle saveState = null;
    protected View view;
    //private HashMap<Integer,View> mapaView;

    protected void startTaskAsync(final Task task){
        new runProcessAsyncOnline(task,getActivity()).start();
    }

    protected void showAlert(final String titulo, final String text){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(getActivity()).setTitle(titulo).setMessage(text).setPositiveButton("OK", null).setCancelable(false).show();
            }
        });
    }

    protected abstract void saveState();
    protected abstract void restoreStateNotNull();

    protected boolean isSaveState(){
        if(saveState != null){
            restoreStateNotNull();
            return true;
        }else{
            saveState = new Bundle();
            return false;
        }
    }

    @Override
    public void onResume() {
        VirtualApplication.getInstance().fragment = this;
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        saveState();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        saveState();
        super.onSaveInstanceState(saveState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState != null){
            saveState = savedInstanceState;
            restoreStateNotNull();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = view;
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        if(savedInstanceState != null){
            if(saveState == null)saveState = savedInstanceState;
        }
    }

    protected void setupToolBar(int id, String title, boolean navgationBack){
        setHasOptionsMenu(true);
        Toolbar toolbar = (Toolbar) view.findViewById(id);
        toolbar.setTitle(title);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ActionBar ac = ((AppCompatActivity)getActivity()).getSupportActionBar();
        ac.setDisplayHomeAsUpEnabled(navgationBack);
    }

    protected View viewForID(int ID){
        return view.findViewById(ID);
    }

    protected ViewGroup viewGroupForID(int ID){
        return (ViewGroup) view.findViewById(ID);
    }

    protected void goneView(int idView){
        ((View) getActivity().findViewById(idView)).setVisibility(View.GONE);
    }

    protected void visibleView(int idView){
        ((View) getActivity().findViewById(idView)).setVisibility(View.VISIBLE);
    }
}
