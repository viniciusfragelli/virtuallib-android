package br.com.virtualtechnology.virtuallib.prefs;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

import br.com.virtualtechnology.virtuallib.R;
import br.com.virtualtechnology.virtuallib.VirtualApplication;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by viniciussenff on 01/03/18.
 */

public class Prefs {

    public static void setString(String key, String value){
        getEditor().putString(key,value).apply();
    }

    public static String getString(String key, String defaultValue){
        return getSharedPreferences().getString(key,defaultValue);
    }

    public static void setBoolean(String key, boolean value){
        getEditor().putBoolean(key,value).apply();
    }

    public static boolean getBoolean(String key, boolean defaultValue){
        return getSharedPreferences().getBoolean(key,defaultValue);
    }

    public static void setFloat(String key, float value){
        getEditor().putFloat(key,value).apply();
    }

    public static float getFloat(String key, float defaultValue){
        return getSharedPreferences().getFloat(key,defaultValue);
    }

    public static void setLong(String key, long value){
        getEditor().putLong(key,value).apply();
    }

    public static long getLong(String key, long defaultValue){
        return getSharedPreferences().getLong(key,defaultValue);
    }

    public static void setInt(String key, int value){
        getEditor().putInt(key,value).apply();
    }

    public static int getInt(String key, int defaultValue){
        return getSharedPreferences().getInt(key,defaultValue);
    }

    private static SharedPreferences.Editor getEditor(){
        return getSharedPreferences().edit();
    }

    private static SharedPreferences getSharedPreferences(){
        AppCompatActivity app = VirtualApplication.getInstance().activity;
        return app.getSharedPreferences(app.getString(R.string.app_name), MODE_PRIVATE);
    }

}
