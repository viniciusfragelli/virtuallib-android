package br.com.virtualtechnology.virtuallib.runs;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;

import br.com.virtualtechnology.virtuallib.animete.Animate;
import br.com.virtualtechnology.virtuallib.runs.utils.Task;


/**
 * Created by vinic on 09/10/2017.
 */

public class runProcessAsyncOnline extends Thread {

    private Task task;
    private View viewTela;
    private View viewProgressBar;
    private ViewGroup viewGroupFatherForAnimete;
    private Exception isError = null;
    private Activity activity = null;
    private Runnable runnable = null;

    public runProcessAsyncOnline(Task task, Activity activity) {
        this.task = task;
        task.setAttributes();
        this.activity = activity;
        setupIDs();
        //colocado aqui pois a instancia dessa run é mais rapido aqui
        runnable = new Runnable() {
            @Override
            public void run() {
                onShowProgressBar();
            }
        };
        getActivity().runOnUiThread(runnable);
    }

    public void run(){
        try {
            runnable = new Runnable() {
                @Override
                public void run() {
                    onPreExecute(); synchronized(this){this.notify();}
                }
            };
            synchronized (runnable) { getActivity().runOnUiThread(runnable); runnable.wait(); }

            doInBackground();

            runnable = new Runnable() {
                @Override
                public void run() {
                    onPostExecute(); synchronized(this){this.notify();}
                }
            };
            synchronized (runnable) { getActivity().runOnUiThread(runnable); runnable.wait(); }

            runnable = new Runnable() {
                @Override
                public void run() {
                    onShowActivity();
                }
            };
            getActivity().runOnUiThread(runnable);
        }catch (InterruptedException e){
            isError = e;
            runnable = new Runnable() {
                @Override
                public void run() {
                    task.onError(isError);
                }
            };
            getActivity().runOnUiThread(runnable);
        }
    }

    private void setupIDs(){
        if(task.idProgressBar != null) {
            if(task.viewLocation == null){
                viewProgressBar = activity.findViewById(task.idProgressBar);
            }else{
                viewProgressBar = task.viewLocation.findViewById(task.idProgressBar);
            }
        }
        if(task.idTela != null) {
            if(task.viewLocation == null){
                viewTela = activity.findViewById(task.idTela);
            }else{
                viewTela = task.viewLocation.findViewById(task.idTela);
            }
        }
        if(task.idFatherForAnimate != null){
            if(task.viewLocation == null){
                viewGroupFatherForAnimete = (ViewGroup) activity.findViewById(task.idFatherForAnimate);
            }else{
                viewGroupFatherForAnimete = (ViewGroup) task.viewLocation.findViewById(task.idFatherForAnimate);
            }
        }
    }

    private void onShowProgressBar(){
        if(viewProgressBar != null){
            viewProgressBar.setVisibility(View.VISIBLE);
        }
        if(viewTela != null){
            viewTela.setVisibility(View.GONE);
        }
        if(viewGroupFatherForAnimete != null) Animate.animeteView(viewGroupFatherForAnimete);
    }

    private void onShowActivity(){
        if(isError != null && !task.forcedShowTela)return;
        if(viewProgressBar != null){
            viewProgressBar.setVisibility(View.GONE);
        }
        if(viewTela != null){
            viewTela.setVisibility(View.VISIBLE);
        }
        if(viewGroupFatherForAnimete != null) Animate.animeteView(viewGroupFatherForAnimete);
    }

    private void onPreExecute() {
        try {
            task.preExcute();
        }catch (Exception ex){
            if(isError == null)isError = ex;
        }
    }

    private void doInBackground() {
        try {
            if(isError == null)task.execute();
        }catch (Exception ex){
            if(isError == null)isError = ex;
        }
    }

    private void onPostExecute() {
        try {
            if(isError == null)task.updateView();
        }catch (Exception ex){
            if(isError == null)isError = ex;
        }

        if(isError != null)task.onError(isError);
    }

    private Activity getActivity(){
        return activity;
    }

}
