package br.com.virtualtechnology.virtuallib.runs.utils;

import android.view.View;

/**
 * Created by vinic on 03/10/2017.
 */

public abstract class Task {

    public Integer idTela = null;
    public Integer idProgressBar = null;
    public View viewLocation;
    public Integer idFatherForAnimate;
    public boolean forcedShowTela = false;


    public Task(Integer idProgressBar, Integer tela){
        this.idTela = tela;
        this.idProgressBar = idProgressBar;
    }

    public Task(Integer idProgressBar, Integer tela, boolean forcedShowTela){
        this.idTela = tela;
        this.idProgressBar = idProgressBar;
        this.forcedShowTela = forcedShowTela;
    }

    public Task(Integer idProgressBar) {
        this.idProgressBar = idProgressBar;
    }

    public Task(Integer idProgressBar, boolean forcedShowTela) {
        this.idProgressBar = idProgressBar;
        this.forcedShowTela = forcedShowTela;
    }

    public Task(Integer idProgressBar, View view) {
        this.idProgressBar = idProgressBar;
        this.viewLocation = view;
    }

    public Task(Integer idProgressBar, View view, boolean forcedShowTela) {
        this.idProgressBar = idProgressBar;
        this.viewLocation = view;
        this.forcedShowTela = forcedShowTela;
    }

    public Task(Integer idProgressBar, Integer tela, View view) {
        this.idTela = tela;
        this.idProgressBar = idProgressBar;
        this.viewLocation = view;
    }

    public Task(Integer idProgressBar, Integer tela, View view, boolean forcedShowTela) {
        this.idTela = tela;
        this.idProgressBar = idProgressBar;
        this.viewLocation = view;
        this.forcedShowTela = forcedShowTela;
    }

    public Task(){

    }

    public Task(Integer idProgressBar, Integer tela, View view, Integer idFatherForAnimate){
        this.idTela = tela;
        this.idProgressBar = idProgressBar;
        this.viewLocation = view;
        this.idFatherForAnimate = idFatherForAnimate;
    }

    public Task(Integer idProgressBar, Integer tela, View view, Integer idFatherForAnimate, boolean forcedShowTela){
        this.idTela = tela;
        this.idProgressBar = idProgressBar;
        this.viewLocation = view;
        this.idFatherForAnimate = idFatherForAnimate;
        this.forcedShowTela = forcedShowTela;
    }

    public Task(Integer idProgressBar, Integer tela, Integer idFatherForAnimate){
        this.idTela = tela;
        this.idProgressBar = idProgressBar;
        this.idFatherForAnimate = idFatherForAnimate;
    }

    public Task(Integer idProgressBar, Integer tela, Integer idFatherForAnimate, boolean forcedShowTela){
        this.idTela = tela;
        this.idProgressBar = idProgressBar;
        this.idFatherForAnimate = idFatherForAnimate;
        this.forcedShowTela = forcedShowTela;
    }

    public void setForcedShowTela(boolean forcedShowTela) {
        this.forcedShowTela = forcedShowTela;
    }

    public void preExcute() throws Exception {}

    public abstract void execute() throws Exception;

    public void updateView() throws Exception {}

    public abstract void onError(Exception ex);

    public void setAttributes(){}
}
